---
title: 'Tools Page 2:'
weight: 0
excerpt: lorem-ipsum
seo:
  title: ''
  description: ''
  robots: []
  extra: []
  type: stackbit_page_meta
template: docs
---
# Paste Excel To Markdown Table:


<iframe height="300" style="width: 100%;" scrolling="no" title="Excel To Markdown Table" src="https://codepen.io/bgoonz/embed/JjNaPpL?default-tab=result&theme-id=dark" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/bgoonz/pen/JjNaPpL">
  Excel To Markdown Table</a> by Bryan C Guner (<a href="https://codepen.io/bgoonz">@bgoonz</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>
